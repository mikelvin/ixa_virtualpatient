categories: "dict[str,str]" = {
    "MTF" : "00",
    "PSN" : "01",
    "ATD" : "04",
    "HDM" : "05",
    "MDV" : "02",
    "TRT" : "03",
    "UNK" : "06"
}
MTF_subcat: "dict[str,str]" = {"sujetconsultation_objet": "000"}
PSN_subcat = {
    "personal_adresse": "000",
    "personal_etat_civil": "001", 
    "personal_enfant": "002", 
    "personal_autre": "003", 
    "personal_age": "004", 
    "personal_profession": "005", 
    "personal_nom": "006", 
    "personal_origine": "007", 
    "personal_poids": "008"
}
ATD_subcat = {
    'antecedent_maladie': '000',
    'antecedent_contraceptions': '001',
    'antecedent_chrirugie': '002',
    'antecedent_autre': '003',
    'antecedent_grossesse': '004',
    'antecedent_allergie': '005', 
    'antecedent_famille': '006', 
    'antecedent_examen': '007', 
    'antecedent_regime': '008'
}
HDM_subcat = {
    'symptome_debut': '000',
    'symptome_fievre': '001',
    'symptome_changement': '002',
    'symptome_type': '003',
    'symptome_description': '004',
    'symptome_localisation': '005',
    'symptome_evolution': '006',
    'symptome_autre': '007'
}
MDV_subcat = {
    'modevie_addictions': '000',
    'modevie_voyages': '001',
    'modevie_sport': '002',
    'modevie_autre': '003',
    'modevie_adresse': '004',
    'modevie_habitation': '005',
}

TRT_subcat = {
    'traitement_mode': '000',
    'traitement_type': '001',
}
UNK_subcat = {'unknow_autre':'000'}

cat_subcat_link = {
    "MTF" : MTF_subcat,
    "PSN" : PSN_subcat,
    "ATD" : ATD_subcat,
    "HDM" : HDM_subcat,
    "MDV" : MDV_subcat,
    "TRT" : TRT_subcat,
    "UNK" : UNK_subcat
}