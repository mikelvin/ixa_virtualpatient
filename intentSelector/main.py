from distutils.ccompiler import gen_preprocess_options
import json
from pathlib import Path
from this import d
from typing import Dict, Iterable, Iterator, List
from categories import cat_subcat_link, categories
from matplotlib.colors import cnames
from regex import P, Pattern
from definitions import ROOT_DIR
import re
from datetime import datetime
import os

file_selection_regex = ".*dialogues_.*$"
data_path = "C:/Users/laord/ixa/full_labforsims2/labforsims2/labforsims2-corpus-master_translated"
relative_target_path = "resources/antolatuta"
target_path = Path(ROOT_DIR, relative_target_path)

global questionCode_response_table
questionCode_response_table = {}

# output:str = ""
# with open(os.join(ROOT_DIR, "src/categories.json"), 'r') as f:
#     output = f.read()

# categories = json.loads(output)

def main():
    init_category_directories(cat_subcat_link)
    loadQuestionResponseTable(Path(data_path, "single/d_p.txt"),Path(data_path, "single/patient_t_dpl.txt"))

    dialog_files_to_process: RegexCollector = RegexCollector(file_selection_regex)
    dirpath:str ; filenames: "list[str]"
    for dirpath, _, filenames in os.walk(data_path):
        for filename in filenames:
            file_path = Path(dirpath, filename)
            dialog_files_to_process.add_if_match(str(file_path))
   
    for file_path in dialog_files_to_process.filePathList:
        print(f"Processing file: {file_path}")
        file_content:"List[str]" = []
        with open(file_path, 'r') as f:
            file_content = f.read().splitlines()
            
        for line in file_content:
            processLineDialogue(line)
    
    file_content:"List[str]" = []
    with open(Path(data_path,"single","doctor_t_dpl.txt"), 'r') as f:
        file_content = f.read().splitlines()
        
    for line in file_content:
        processLineSingleDoctor(line)
    
def init_category_directories(pcategories:"dict[str,dict]"):
    if not target_path.exists():
        target_path.mkdir(parents=True)
    for cat in pcategories.keys():
        tag_path =  Path(target_path, cat)
        tag_path.mkdir(exist_ok=True)
        for subcat in pcategories[cat].keys():
            subt_tag_path = Path(tag_path, subcat)
            subt_tag_path.mkdir(exist_ok=True)

class RegexCollector():
    def __init__(self, regex: str) -> None:
        self.filePathList: "list[str]" = []
        self.regex: str = regex

    def __iter__(self) -> "Iterator[str]":
        return iter(self.filePathList)

    def get_matches(self) -> "list[str]":
        """Get the list of files that match the regex set on the construction of this object

        Returns:
            List[str]: List of the string representations matching the regex
        """
        return self.filePathList

    def add_if_match(self, ptext:str) -> bool:
        """Adds a text string to it's internal list if the internal regex matches the text

        Args:
            pfile_path (str): The string that wants to be added to the collection / internal_list

        Returns:
            bool: It returns true if the ptext has been added. If not, it returns false.
        """
        if re.match(self.regex, ptext) is None:
            return False

        self.filePathList.append(ptext)
        return True

def processLineDialogue(line:str):
    #  (d|p)[0-9]{2}[0-9]{2}[0-9]{3}[0-9]{1,}[ ][^\n]+\n
    # print (f"Analyzing line: {line}")
    category_code = line[3:5]
    sub_category_code = line[5:8]
    # print (f"Category code: {category_code}")
    # print (f"Subcategory code: {sub_category_code}")
    
    insert_on_path = get_category_path(category_code, sub_category_code)

    with open(Path(insert_on_path, "dialogues.txt"), '+a') as f:
        f.write(line+"\n")

def processLineSingleDoctor(line:str):
    #  (d|p)[0-9]{2}[0-9]{2}[0-9]{3}[0-9]{1,}[ ][^\n]+\n
    # print (f"Analyzing single file line: {line}")
    category_code = line[0:3]
    # print (f"Category code: {category_code}")
    question_id = line.split(" +++ ")[0]
    if category_code in categories:
        category_code = categories[category_code]

    insert_on_path = get_category_path(category_code, None)

    with open(Path(insert_on_path, "single.txt"), '+a') as f:
        f.write(line+"\n")
        f.write(getAnswerOfPatient(question_id)+"\n")

def getAnswerOfPatient(question_code:str):
    return questionCode_response_table[question_code]

def loadQuestionResponseTable(link_file:str, patient_file:str):
    global questionCode_response_table 
    questionCode_response_table = {}

    with open(link_file, 'r') as f:
        lines = f.read().splitlines()
        dcode_pcode = [i.split(" +++ ") for i in lines]

    patient_code_sentence: dict[str,str] = {}
    with open(patient_file, 'r') as f:
        lines = f.read().splitlines()
        pcode_psent: List[List[str,str]] = [i.split(" +++ ") for i in lines]
        for pcode, psent in pcode_psent:
            patient_code_sentence[pcode] = pcode + " +++ " + psent

    for dcode, pcode in dcode_pcode:
        questionCode_response_table[dcode] = patient_code_sentence[pcode]


def get_category_path(category_code:str, sub_category_code:"str|None"):
    a = [k for k in categories if categories[k] == category_code]
    if len(a) == 0:
        return
    category_tag = a[0]
    insert_on_path = Path(target_path, category_tag)
    sub_categories = cat_subcat_link[category_tag]
    b = [k for k in sub_categories if sub_categories[k] == sub_category_code]
    if len(b) != 0:
        sub_category = b[0]
        insert_on_path = Path(insert_on_path, sub_category)
    return insert_on_path

main()
