""" Setup configuration file. paketeak sortzeko erabilia
"""
from setuptools import setup # type: ignore

setup(
    name='corp_transl',
    version='1.0',
    install_requires=[
        "deepl",
        "deep_translator"
    ],
    include_package_data=True,
    
    # packages=find_packages()
)
