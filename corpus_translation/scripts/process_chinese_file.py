
from corp_transl.string_replacer.jsonStrIdent import CodeManager, CodeReplacer, ShaCodeGen
from corp_transl.translators.MydeeplTranslator import MyDeepLTranslator, ResponseError
from corp_transl.file_divider.structurer import BifurcSplitNode, DocumentVerticalSpliter
from pathlib import Path

from datetime import datetime 
import os

from config import DEEPL_API_KEY, ROOT_DATA_PATH

# root_data_path = Path("/home/laordenmikel/git/ixa_virtualpatient/corpus_translation/my_resources/chinese_corp")
rdp = Path(ROOT_DATA_PATH)
data_path = rdp.joinpath("dataset")
result_path = rdp.joinpath("processed")
aux_path = rdp.joinpath("auxi")

code_gen = ShaCodeGen()

old_store = CodeManager(code_gen)
translated_store = CodeManager(code_gen)

code_engine = CodeReplacer(old_store)

SIMPLE_FILE_NAME = "val_dist"

MAPPINGS_FILE_NAME = f"{SIMPLE_FILE_NAME}_mapings.csv"
CODED_FILE_NAME = f"{SIMPLE_FILE_NAME}_CODED.json"
TARGET_FILE_NAME = f"{SIMPLE_FILE_NAME}.json"
TRANSLATED_MAPINGS_FILE_NAME = f"{SIMPLE_FILE_NAME}_mapings_t.csv"

def create_mappings():
    data = ""
    with open(Path(data_path,TARGET_FILE_NAME), "r", encoding="utf-8") as f:
        data = f.read()

    with open(Path(aux_path, CODED_FILE_NAME), "+w", encoding="utf-8") as f:
        f.write(code_engine.replacing_function(data))

    with open(Path(aux_path, MAPPINGS_FILE_NAME), "+w", encoding="utf-8") as f:
        f.write(old_store.store_mapping())

def translate_mappings():
    with open(Path(aux_path, MAPPINGS_FILE_NAME), "r", encoding="utf-8") as f:
        mapping_data = f.read()
    
    divider = DocumentVerticalSpliter(BifurcSplitNode(" "), mapping_data)
    
    slice_to_translate = divider.get(1)
    
    translator = MyDeepLTranslator(DEEPL_API_KEY)
    
    try:
        translated_text = translator.translateTX2Es(slice_to_translate)
    except ResponseError as r:
        translated_text = r.partialAns
    file_root, file_extension = os.path.splitext(MAPPINGS_FILE_NAME)
    f_name_format = "{f_root}_t_{tr_id}_{timestamp}{f_ext}"
    translated_file_name = f_name_format.format(
        f_root      = file_root,
        tr_id       = translator.identif,
        timestamp   = str(int(datetime.today().timestamp())),
        f_ext       = file_extension)
    
    with open(Path(aux_path, translated_file_name), "+w", encoding="utf-8") as f:
        f.write(translated_text)
        
    divider.set(1,translated_text)
    
    with open(Path(aux_path, TRANSLATED_MAPINGS_FILE_NAME), "+w", encoding="utf-8") as f:
        f.write(divider.reconstruct())

def rebuild_from_mappings():
    maping_data = ""
    with open(Path(aux_path, TRANSLATED_MAPINGS_FILE_NAME), "+r", encoding="utf-8") as f:
        maping_data = f.read()
    coded_file_data = ""
    with open(Path(aux_path, CODED_FILE_NAME), "+r", encoding="utf-8") as f:
        coded_file_data = f.read()
    
    translated_store.load_mapping(maping_data)
    uncoded_translated_file  = CodeReplacer(translated_store).rebuilding_function(coded_file_data)
    
    with open(Path(result_path, TARGET_FILE_NAME), "+w", encoding="utf-8") as f:
        f.write(uncoded_translated_file)

# create_mappings()
# translate_mappings()
# rebuild_from_mappings()







def rebuild_translatedmaping_from_translated_sentences():
    mapping_data = ""
    translated_sentences_file_name = "val_dist_mapings_t_dpl_1662830367.csv"
    my_important_path = r""
    
    with open(Path(my_important_path, MAPPINGS_FILE_NAME), "r", encoding="utf-8") as f:
        mapping_data = f.read()
    
    divider = DocumentVerticalSpliter(BifurcSplitNode(" "), mapping_data)

    translated_text = ""
    with open(Path(my_important_path, translated_sentences_file_name), "+r", encoding="utf-8") as f:
        translated_text = f.read()
        
    divider.set(1,translated_text)
    
    with open(Path(my_important_path, TRANSLATED_MAPINGS_FILE_NAME), "+w", encoding="utf-8") as f:
        f.write(divider.reconstruct())

