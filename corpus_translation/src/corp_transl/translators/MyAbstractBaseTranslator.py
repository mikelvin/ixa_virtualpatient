from abc import ABC, abstractmethod
from typing import List

class MyAbstractBaseTranslator(ABC):

    @abstractmethod
    def __init__(self, api_key:str):
        self.api_key = api_key

    @property
    @abstractmethod
    def name(self) -> str:
        ...
    
    @property
    @abstractmethod
    def identif(self) -> str:
        ...

    @abstractmethod
    def translateFr2Es(self, ptext:str)->str:
        ...

    @abstractmethod
    def translateTX2Es(self, ptext:str)->str:
        ...
    
    @abstractmethod
    def translate(self, from_lang:str, to_lang:str, text:str) -> str:
        ...

class ResponseError(Exception):
    def __init__(self, origException: Exception, message: str, partialAns=None) -> None:
        self.orig_exception: Exception = origException
        self.message: str = message
        self.partialAns = partialAns
    def __str__(self) -> str:
        return f"ResponseError: Bad response - {self.message}. Original exception: {str(self.orig_exception)}"

class ConfigError(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


def split_lines_in_blocks(text:str, maxCharBlock:int) -> 'List[str]':
    must_split = len(text) > maxCharBlock
    blocks = []
    if not must_split:
        blocks.append(text)
    else:
        current_block = ""
        for line in text.splitlines(keepends=True):
            if len(current_block) + len(line) >= maxCharBlock:
                if len(current_block) != 0:
                    blocks.append(current_block)
                current_block = line
            else:
                current_block += line
        blocks.append(current_block)
    return blocks