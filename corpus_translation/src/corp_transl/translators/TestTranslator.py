from . import MyAbstractBaseTranslator as mabt

class TestTranslator(mabt.MyAbstractBaseTranslator):
    def __init__(self, api_key: str):
        super().__init__(api_key)
    @property
    def name(self) -> str:
        return "test_translator"
    
    @property
    def identif(self) -> str:
        return "testtr"
    
    def translate(self, from_lang: str, to_lang: str, text: str) -> str:
        extra : str = ""
        tr: "list[str]" =  map(lambda line: extra + line, text.splitlines(keepends=True))
        return "".join(tr)
    
    def translateFr2Es(self, ptext: str) -> str:
        return self.translate(None, None, ptext)
    
    def translateTX2Es(self, ptext:str)->str:
        return self.translate(None, None, ptext)
    