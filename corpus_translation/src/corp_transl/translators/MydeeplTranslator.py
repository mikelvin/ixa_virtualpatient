from typing import List
import deepl

from corp_transl.translators.MyAbstractBaseTranslator import MyAbstractBaseTranslator, split_lines_in_blocks, ResponseError
import time

class MyDeepLTranslator(MyAbstractBaseTranslator):
    BOUNDARIES = {
            'MAX_CALL_CHARS': 2000
        }
    @property
    def name(self):     
        return 'deepl'

    @property
    def identif(self):
        return 'dpl'

    def __init__(self, api_key:str) -> None:
        super().__init__(api_key)
        self.trEngine: "deepl.Translator" = deepl.Translator(self.api_key) 

    def translate(self, from_lang: str, to_lang: str, text: str) -> str:
        translated_text_blocks: "List[str]" = []
        text_blocks: "List[str]" = split_lines_in_blocks(text, MyDeepLTranslator.BOUNDARIES['MAX_CALL_CHARS'])  # 
        block: str
        print(f"Begin translating {str(len(text))} characters in {str(len(text_blocks))} blocks")
        for index, block in enumerate(text_blocks):
            try: 
                translated_text: "deepl.TextResult" = self.trEngine.translate_text(text=block, source_lang=from_lang, target_lang=to_lang)
                translated_text_blocks.append(translated_text.text)
                print(f"Block {str(index)} translated - {str(len(block))} chars")
                time.sleep(5)
            except Exception as e:
                print(f"End translation with errors. On block {str(index)} of {str(len(text_blocks))}")
                print(e)
                raise ResponseError(e, str(e), ''.join(translated_text_blocks))
        # json.dumps(response, sort_keys=True, ensure_ascii=False, indent=4, separators=(',', ': '))
        print("End translation")
        return ''.join(translated_text_blocks)

    def translateFr2Es(self, ptext: str) -> str:
        return self.translate("FR","ES", ptext)
    
    def translateTX2Es(self, ptext: str) -> str:
        return self.translate("ZH","ES", ptext)
