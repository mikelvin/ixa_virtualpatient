from corp_transl.translators.MyAbstractBaseTranslator import MyAbstractBaseTranslator, split_lines_in_blocks, ResponseError
from typing import List
from deep_translator import microsoft # type: ignore

import requests, uuid 

class MicrosoftTranslator(MyAbstractBaseTranslator):
    def __init__(self, api_key:str):
        super(api_key)
        self.BOUNDARIES = {
            'MAX_CALL_CHARS': 5000
        }
        # Add your subscription key and endpoint
        subscription_key = None # TODO: Set the api key properly 
        endpoint = "https://api.cognitive.microsofttranslator.com"
        # Add your location, also known as region. The default is global.
        # This is required if using a Cognitive Services resource.
        location = "westeurope"

        path = '/translate'
        self.constructed_url = endpoint + path

        self.params = {
            'api-version': '3.0',
            'from': 'fr',
            'to': ['es']
        }

        self.headers = {
            'Ocp-Apim-Subscription-Key': subscription_key,
            'Ocp-Apim-Subscription-Region': location,
            'Content-type': 'application/json; charset=UTF-8',
            'X-ClientTraceId': str(uuid.uuid4())
        }

    def translate(self, text:str) -> str:
        #ema = tr.translate(text)
        return ""

    def translateFr2Es(self, text: str) -> str:
        """Translate text from French to Spanish making multiple calls if needed.

        Args:
            text (str): _description_

        Returns:
            str: Translated string
        
        Raises:
            BadArgumentUsage: When an error is send as the translation response instead of the translation itself
        """
        translated_text_blocks: "List[str]" = []
        text_blocks = split_lines_in_blocks(text, self.BOUNDARIES['MAX_CALL_CHARS'])  # 
        for block in text_blocks:
            # You can pass more than one object in body.
            body = [{
                'text': block
            }]

            request = requests.post(self.constructed_url, params=self.params, headers=self.headers, json=body)
            response = request.json()
            decodedResponse = response[0]
            if "translations" in decodedResponse:
                decodedResponse = decodedResponse["translations"][0]["text"]
            else:
                raise ResponseError(decodedResponse["error"]["code"], decodedResponse["error"]["message"],''.join(translated_text_blocks))
            
            translated_text_blocks.append(decodedResponse)
        # json.dumps(response, sort_keys=True, ensure_ascii=False, indent=4, separators=(',', ': '))
        return ''.join(translated_text_blocks)

class MyMicrosoftTranslator2(MyAbstractBaseTranslator):
    region: str = 'westeurope'
    BOUNDARIES = {
            'MAX_CALL_CHARS': 5000
        } 

    @property
    def name(self):
        return "microsoft"

    @property
    def identif(self):
        return "mcsft"
    
    def __init__(self, api_key) -> None:
        super(api_key)
        self.tr_engine: "microsoft.MicrosoftTranslator" = microsoft.MicrosoftTranslator(
            api_key = self.api_key, 
            region  = MyMicrosoftTranslator2.region,
            target  = 'es',
            source  = 'fr')
        
    def translateFr2Es(self, ptext: str) -> str:
        translated_text_blocks: "List[str]" = []
        text_blocks: "List[str]" = split_lines_in_blocks(ptext, MyMicrosoftTranslator2.BOUNDARIES['MAX_CALL_CHARS'])  # 
        block: str
        print(f"Begin translating {str(len(ptext))} characters in {str(len(text_blocks))} blocks")
        for block in text_blocks:
            translated_block:str = ''
            try:
                translated_block = self.tr_engine.translate(block)
                translated_text_blocks.append(translated_block)
            except Exception as ex:
                raise ResponseError(
                    ex, 
                    "Error ocurred on translation", 
                    ''.join(translated_text_blocks)
                    )
        print("End translation")
        return ''.join(translated_text_blocks)
