import sys, pathlib
sys.path.insert(1, '../config')
sys.path.insert(1, pathlib.Path("..").absolute())

from typing import List
from definitions import ROOT_DIR
from MyAbstractBaseTranslator import MyAbstractBaseTranslator, split_lines_in_blocks, ResponseError
from Scripts.config import DEEPL_API_KEY
from pathlib import Path
from selenium.webdriver.firefox.service import Service
from selenium.webdriver import FirefoxOptions
from selenium import webdriver

PATH_TO_FIREFOX_DRIVER: str = str(Path(ROOT_DIR, 'libs/geckodriver.exe').resolve())

class SeleniumDeepLTranslator(MyAbstractBaseTranslator):
    BOUNDARIES = {
            'MAX_CALL_CHARS': 5000
        }
    def __init__(self) -> None:
        service = Service(executable_path=PATH_TO_FIREFOX_DRIVER)
        self.options = FirefoxOptions()
        self.driver = webdriver.Firefox(service=service)
        self.driver.get("https://www.deepl.com/translator")

    @property
    def name(self):
        return 'deepl_selenium'

    @property
    def identif(self):
        return 'dpls'

    def translateFr2Es(self):
        pass

if __name__ == "__main__":
    SeleniumDeepLTranslator()