import json
from pathlib import Path
import re
from uuid import uuid3, uuid4, NAMESPACE_DNS
from typing import Any, Callable, Dict, List, Text

pattern = '".*"'
s = 'dfasdf: "afdsfa"'
result = re.sub(pattern,'',s)
print(result)

class CodeGenerator:
    def get_code_for(self, p_text):
        return str(uuid4())

class ShaCodeGen(CodeGenerator):
    def get_code_for(self, p_text):
        return str(uuid3(NAMESPACE_DNS,p_text))

class CodeDataParser:
    defa_format = "{code} {data}" # Aldakorra
    rest_regex = "(?P<code>[^\n\s]+)\s(?P<data>.*)(\n)?" # Aldakorra
    def get_as_string(self, p_code:str, p_data:Any) -> str:
        my_conv_data = self.defa_format.format(
            code = str(p_code),
            data = str(p_data)
        )
        return my_conv_data
    def get_as_tuple(self, p_string:str) :
        match = re.match(self.rest_regex, p_string)
        if match is None:
            raise Exception("Ezin izan dira datuak extraitu: " + p_string)
        
        text_code = match.group("code")
        text_data = match.group("data")
        return text_code, text_data

class CodeManager():
    def __init__(self, p_code_generator:"CodeGenerator|None"=None, formater:"CodeDataParser"=None) -> None:
        self.code_text_dict: 'Dict[str,Text]' = {}
        self.code_generator = p_code_generator
        if p_code_generator == None:
            self.code_generator = CodeGenerator()
        self.formater = formater
        if formater == None:
            self.formater = CodeDataParser()

    def set_code(self, p_text:Text) -> str: #TODO: 
        uneko_code = self.code_generator.get_code_for(p_text)
        self.code_text_dict[uneko_code] = p_text
        return uneko_code

    def get_mapping_as_dict(self) -> 'Dict[str, Text]':
        return self.code_text_dict
    
    def store_mapping(self) -> Text:
        ema = []
        for key in self.code_text_dict:
            ema.append(
                self.formater.get_as_string(key, self.code_text_dict[key])
            )
        return "\n".join(ema)

    def load_mapping(self, p_text:str) -> bool:
        lines = p_text.splitlines()
        for line in lines:
             code, data = self.formater.get_as_tuple(line)
             if code is not None and data is not None:
                self.code_text_dict[code] = data

    def restore_text(self, p_code:str) -> str:
        return self.code_text_dict[p_code]

class CodeReplacer:
    rep_rule_ini = "${"     
    rep_rule_end = "}"

    def __init__(self, code_manager: CodeManager) -> None:
        self.code_manager = code_manager

    def replacing_function(self, p_text: Text) -> Text:
        return self._replacing_rebuilding_helper(p_text, self.generate_code)

    def rebuilding_function(self, p_text:Text) -> Text:
        return self._replacing_rebuilding_helper(p_text, self.restore_code)
    
    def _replacing_rebuilding_helper(self, p_text:Text, rep_res_funct:Callable[[str], str]):
        data_json = json.loads(p_text)
        self._process_json_item(data_json, rep_res_funct)
        return json.dumps(data_json, indent=4, separators=(',', ': '), ensure_ascii=False)

    def _process_json_item(self, p_json_item, p_fun:Callable[[str], str]) -> None:
        if isinstance(p_json_item, Dict):
            self._process_json_dict(p_json_item, p_fun)

        elif isinstance(p_json_item, List):
            self._process_json_array(p_json_item, p_fun)

    def _process_json_array(self, p_list: List[Any], p_fun:Callable[[str], str]) -> None:
        for index, item in enumerate(p_list):
            if isinstance(item, str):
                p_list[index] = p_fun(item)
            else:
                self._process_json_item(item, p_fun)
        
    def _process_json_dict(self, p_dict: Dict[str,Any], p_fun:Callable[[str], str]) -> None:
        for key in p_dict:
            val = p_dict[key]
            if isinstance(val, str):
                p_dict[key] = p_fun(val)
            else:
                self._process_json_item(val, p_fun)
        
    def generate_code(self, p_text: Text) -> Text:
        repl_rule = "{init}{data}{end}"
        return repl_rule.format(
            init = self.rep_rule_ini,
            end = self.rep_rule_end,
            data = self.code_manager.set_code(p_text)
        )
    
    def restore_code(self, p_text: Text) -> Text:
        ma = re.match(r'\$\{(?P<code>.*)\}', p_text)
        if ma != None:
            code = ma.group("code")
            return self.code_manager.restore_text(code)
        return p_text