from multiprocessing.dummy import Array
from pathlib import Path
from typing import List, Dict, Any, Union
from corp_transl.translators.MyAbstractBaseTranslator import MyAbstractBaseTranslator, ResponseError
from corp_transl.translators.MyMicrosoftTranslator import MyMicrosoftTranslator2

import re
from os.path import join
import os
from corp_transl.file_divider import structurer as s
from datetime import datetime

TRANSLATOR: MyAbstractBaseTranslator = MyMicrosoftTranslator2()

#data_path: str = 'resources/slicedCorpus'
#data_path = join(ROOT_DIR, data_path)
data_path = "C:/Users/laord/ixa/full_labforsims2/labforsims2/labforsims2-corpus-master"

file_struct_types: List[dict[str, Any]] = [
    {
        "name": "Dialog files",
        "regex": ".*dialogues$",
        "separator": " ",
        "active": False,
        "translator": TRANSLATOR
    },
    {
        "name": "Patient file",
        "regex": ".*patient\.txt",
        "separator": " +++ ",
        "active": False,
        "translator": TRANSLATOR
    },
    {
        "name": "Doctor file",
        "regex": ".*doctor\.txt",
        "separator": " +++ ",
        "active": False,
        "translator": TRANSLATOR
    }
]

def main(p_data_path:Union[str,Path], p_structs: List[dict[str, Any]]):
    active_structs: "list[Dict[str,Any]]" = [s for s in p_structs if "active" in s and s["active"]]
    paths_4_respective_struct: "list[list[str]]" = [[] for _ in active_structs]
    
    # Search for mathching files
    f_path:str; f_name_list:"list[str]"
    for f_path, _, f_name_list in os.walk(p_data_path):
        # For every file
        for f_name in f_name_list:
            abs_f_name: str = join(f_path,f_name)
            index: int = find_match_with_struct(abs_f_name, active_structs)
            if index is not None:
                paths_4_respective_struct[index].append(abs_f_name)

    # Prepare files 4 translation
    # Read all lines
    file_list:"list[str]"; struct4thisfilelist:"Dict[str,Any]"
    for file_list, struct4thisfilelist in zip(paths_4_respective_struct, active_structs):
        if "separator" not in struct4thisfilelist:
            break
        separator:str = struct4thisfilelist["separator"]
        file:str
        for file in file_list:
            simplify_and_translate_file(file, separator, TRANSLATOR)

def find_match_with_struct(path:str, structs:"List[Dict[str,Any]]") -> "None|int":
    """Retrurn the position of the matching structure for the path that has been given

    Args:
        path (str): the string representation of the file path that wants to be mached with the structures decribed
        structs (List[Dict[str,Any]]): The list of defined file structures 

    Returns:
        None|int: if a match is found, the index of that matchin struct on the structs list. Else, it returns None
    """
    index:int; s:"Dict[str,Any]"
    for index,  s in enumerate(structs):
        if "regex" in s and re.search(s["regex"], path) is not None:
            return index
    return None

def simplify_and_translate_file(file:str, separator:str, t: MyAbstractBaseTranslator):
    # split codes and sentences on 2 files
    code_file_path:str ; sentence_file_path:str
    code_file_path, sentence_file_path = s.createBindingDoc(separator, file)
    
    # Translate the sentence file contents
    translated_sentence_file_name = translate_file_and_store(sentence_file_path, t)

    file_root, file_extension = os.path.splitext(file)
    f_name_format = "{f_root}_t_{tr_id}{f_ext}"
    translated_file = f_name_format.format(
        f_root  = file_root,
        tr_id   = t.identif,
        f_ext   = file_extension)

    s.reconstructDoc(separator, code_file_path, translated_sentence_file_name,  translated_file)

    os.remove(code_file_path)
    os.remove(sentence_file_path)

def translate_file_and_store(file_path:str, ptranslator: MyAbstractBaseTranslator) -> str:
    print(f"Translating {file_path} file. With {ptranslator.name} translator")
    original_text:str = ""
    translated_text:str = ""

    # Get the sentence file contents
    with open(file_path, "r", encoding="utf-8") as f:
        original_text = f.read()
    
    # Translate the file
    try:
        translated_text = ptranslator.translateFr2Es(original_text)
    except ResponseError as e:
        print(f"An error ocurred translating '{file_path}' ")
        if e.partialAns is not None:
            translated_text = e.partialAns
            not_tr_charcters: float = (len(original_text) - len(translated_text)) / len(original_text)*100
            print(f"Only {len(translated_text)} characters of {len(original_text)} have been translated ({str(int(not_tr_charcters))})")
    
    # Write the contents of translated sentence file  
    file_root, file_extension = os.path.splitext(file_path)
    f_name_format = "{f_root}_t_{tr_id}_{timestamp}{f_ext}"
    translated_file_name = f_name_format.format(
        f_root      = file_root,
        tr_id       = ptranslator.identif,
        timestamp   = str(int(datetime.today().timestamp())),
        f_ext       = file_extension)

    with open(translated_file_name, "w", encoding="utf-8") as f:
        f.write(translated_text)

    return translated_file_name
    
if __name__ == "__main__":
    main(data_path, file_struct_types)