# Fitxategiak berrantolatzen dituen metodo sorta
from abc import ABC, abstractmethod
from operator import mod
import os
from typing import List, Optional, Tuple
import numpy.typing as npt
import numpy as np
import pandas as pd

import logging


def createBindingDoc(codeDivider:str, filePath:str) -> Tuple[str,str]:
    """
        returns (codefile, sentenceFile)
    """
    # Fitxategiaren lerro bakoitzeko kodea eta esaldia banatu "codeDivider" erabiz banatzaile bezala
    # Gainera erablgarriak ez diren hutsuneak kendu (hasieran eta amaieran daudenak)
    codesbyline = []
    sentencebyline = []
    with open(filePath, 'r', encoding="utf-8") as f: 
        for line in f.readlines():
            splitedLine = line.split(codeDivider,1)

            codesbyline.append(splitedLine[0].strip())
            sentencebyline.append(splitedLine[1].strip())

    # Kodeen fitxategia sortu
    fileRotExt = os.path.splitext(filePath)
    codesFileName = fileRotExt[0]+"_codes"+fileRotExt[1]
    with open(codesFileName, "+w", encoding="utf-8") as f:
        f.write("\n".join(codesbyline))
    # Esaldien fitxategia sortu
    simplFileName = fileRotExt[0]+"_simpl"+fileRotExt[1]
    with open(fileRotExt[0]+"_simpl"+fileRotExt[1], "w", encoding="utf-8") as f:
        f.write("\n".join(sentencebyline))

    return codesFileName, simplFileName


def reconstructDoc(codedivider:str, codeFilePath:str, sentenceFilePath:str, newFileName:str):
    codeArray: "list[str]" = []
    sentenceArray: "list[str]" = []

    with open(codeFilePath, '+r', encoding="utf-8") as f:
        codeArray = f.readlines()

    with open(sentenceFilePath, '+r', encoding="utf-8") as f:
        sentenceArray = f.readlines()

    if len(codeArray) != len(sentenceArray):
        raise Exception("Los Codigos no concuerdan con las frases")

    # Join codes with sentences
    joinedArray: "list[str]" = []
    for lineNum, code in enumerate(codeArray, start=0):
        joinedArray.append(code.strip("\n") + codedivider +  sentenceArray[lineNum].strip("\n"))

    with open(newFileName, "+w", encoding="utf-8") as f:
        f.write("\n".join(joinedArray))



    

class SplitNode(ABC):
    def __init__(self, divisor: str) -> None:
        super().__init__()
        self._divisor : str = divisor
    
    @abstractmethod
    def split(self, text: str) -> list[str]:
        ...

class BifurcSplitNode(SplitNode):
    def __init__(self, divisor: str, right: Optional[SplitNode] = None, left: Optional[SplitNode] = None):
        super().__init__(divisor)
        self._right: SplitNode = right
        self._left: SplitNode = left
        
    def split(self, text: str) -> List[str]:

        splited_line = text.split(self._divisor, maxsplit=1)

        r_ema = [splited_line[0]]
        l_ema = []
        if self._right is not None:
            r_ema = self._right.split(splited_line[0])    
        
        if len(splited_line) == 2:
            # Añade el divisor en la emaitza
            r_ema.append(self._divisor)
            
            l_ema = [splited_line[1]]
            if self._left is not None:
                l_ema = self._left.split(splited_line[1])       
        r_ema.extend(l_ema)
        return r_ema
 
class MultiSplitNode(BifurcSplitNode): 
    def __init__(self, divisor: str, apply_to_all: Optional[SplitNode] = None) -> None:
        super().__init__(divisor, right=apply_to_all, left=self)
        

    def split(self, data):
        print(data)
        return super().split(data)

class DocumentVerticalSpliter:
    def __init__(self, split_pattern: SplitNode, data:str) -> None:
        self._split_pattern = split_pattern
        self._banatzaileen_lista = []
        self._data_col_number = 0
        
        lines = data.splitlines()
        if len(lines):
            sample_row = self._split_pattern.split(lines[0])
            self._banatzaileen_lista = [s for i , s in enumerate(sample_row) if i%2]
            self._data_col_number = len(self._banatzaileen_lista) + 1
        
        data_list: list[list[str]] = []
        index:str; line:str
        for index, line in enumerate(lines):
            line_split: list[str] = self._split_pattern.split(line)
            data_line = [s for i, s in enumerate(line_split) if not i%2 ]
            
            if len(data_line) == self._data_col_number:
                data_list.append(data_line)
            else:
                logging.warning("la linea {0} con longitud {1} no se ha podido procesar".format(index,  len(data_line)))
        
        self._data_row_number = len(data_list)
        self._data_array = np.array(data_list, dtype=np.object_)

            
    def get(self, index):
        return '\n'.join([str(num) for num in self._data_array[:, index]])
    
    def set(self, index:int, data_column: str):
        lines = data_column.splitlines()
        if len(lines) != self._data_row_number:
            raise Exception("No se puede añadir la columna porque no encaja en la estructura inicializada")
        self._data_array[:,index] = lines
        
    def reconstruct(self) -> str:
        data = []
        for row in self._data_array:
            line = ""
            for index, col in enumerate(row):
                line += str(col)
                if index < len(self._banatzaileen_lista):
                    line += str(self._banatzaileen_lista[index])
            data.append(line)
        return "\n".join(data)
    
if __name__ == "__main__":
    """
    spl = DocumentVerticalSpliter(BifurcSplitNode(" "))       
    with open("/home/laordenmikel/git/ixa_virtualpatient/corpus_translation/data.txt") as f:
        file_data = f.read()
        
    spl.load_data(file_data)
    print(spl.reconstruct())
    spl.set(1,"\n".join(["6","7","8","9","10"]))
    print(spl.reconstruct())
    """
    print("A")
    print(MultiSplitNode(" ").split("HOla QUe tal"))
    
    # print(BifurcSplitNode(" ").split("aa aa"))